<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="../resepsionis/resepsionis.php">
            <i class="fa fa-book"></i> <span>Data Pemesan</span>
          </a>
        </li>
      </ul>
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="../fasres/fasres.php">
            <i class="fa fa-home"></i> <span>Fasilitas Kamar</span>
          </a>
        </li>
      </ul>
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="../fashotel/fashotel.php">
            <i class="fa fa-bed"></i> <span>Fasilitas Hotel</span>
          </a>
        </li>
      </ul>
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="../../logOut.php">
            <i class="fa fa-windows"></i> <span>Logout</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
</aside>