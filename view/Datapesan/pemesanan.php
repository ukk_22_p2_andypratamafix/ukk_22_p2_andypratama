<?php
    require_once '../../Models/pemesanan.php';
    $pemesanan = new pemesana();
    $result = $pemesanan->get_data();

    if(isset($_POST['submit']) and $_POST['submit']=='reservasi'){
        require_once '../../Models/Pemesanan.php';
        $pemesanan = new Pemesanan();

        $nm_pemesanan = $_POST['nm_pemesanan'];
        $email = $_POST['email'];
        $no_hp = $_POST['no_hp'];
        $nm_tamu = $_POST['nm_tamu'];
        $id_kamar = $_POST['id_kamar'];
        $cek_in = $_POST['cek_in'];
        $cek_out = $_POST['cek_out'];
        $jml = $_POST['jml'];

        if(empty($nm_pemesanan) AND empty($id_kamar)){
            $alert = "<script>alert('Silahkan Lengkapi Data')</script>";
            echo $alert;
        }else{
            $result = $pemesanan->insert($nm_pemesanan, $email, $no_hp, $nm_tamu, $id_kamar, $cek_in, $cek_out, $jml);
            if(isset($result)){
                header("Location:".BASE_URL."bukti_reservasi.php");
                exit();
            }
        }

    }
?>
<!-- Form Tambah -->
         

<!-- jQuery -->
<script src="../../Public/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../Public/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../Public/js/adminlte.min.js"></script>
<script src="data.js"></script>
</body>
</html>
