var table;

function init() {
    showForm(false);
    get_data();

    $("#formTambah").on("submit", function (e) {
        saveOrEdit(e);
    });
}

// Clean Form
function cleanString() {
    $("#id_kamar").val("");
    $("#tipe_kamar").val("");
    $("#jml").val("");
}

// Untuk menampilkan Form
function showForm(flag) {
    cleanString();
    if (flag) {
        $("#daftarKamar").hide();
        $("#formTambah").show();
        $("#btnTambah").hide();
    } else {
        $("#daftarKamar").show();
        $("#formTambah").hide();
        $("#btnTambah").show();
    }
}

// Untuk membatalkan showForm
function closeForm() {
    cleanString();
    showForm(false);
}

// Untuk menampilkan seluruh dataTable
function get_data(){
    table = $('#tbl_list').dataTable({
        "aProcessing" : true,
        "aServerSide" : true,
        "ajax" : {
            url : '../../controller/pemesanan.php?action=get_data',
            type : "POST",
            dataType: "json",
            error: function (e){
                console.log(e.responseText);
            }
        },
        responsive: true
    }).DataTable();
}

function saveOrEdit(e) {
    e.preventDefault();
    var formData = new FormData($("#formTambah")[0]);

    if ($('#tipe_kamar').val() == '' || $('#jml').val() == ''){
        swal("Perhatian", "Silahkan Isi Data Terlebih Dahulu", "warning");
    } else {
        $.ajax({
            url: "../../Controller/pemesanan.php?action=saveOrEdit",
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,

            success: function (data) {
                swal("Selamat", "Data Berhasil Disimpan", "success");
                showForm(false);
                table.ajax.reload();
            }
        });
    }
    cleanString();
}

function show(id_pemesanan){
    $.post("../../Controller/pemesanan.php?action=show", {id_pemesanan: id_pemesanan}, function (data){
        data = JSON.parse(data);
        showForm(true);

        $("#id_pemesanan").val(data.id_pemesanan);
        $("#nm_tamu").val(data.nm_tamu);
        $("#jml").val(data.jml);
    })
}

function delete_data(id_pemesanan){
    swal({
        title: "Konfirmasi Penghapusan Data",
        text: "Data Akan Dihapus Permanen!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
            if (willDelete) {
                $.post("../../Controller/pemesanan.php?action=delete_data", {id_pemesanan: id_pemesanan},
                function (data) { })

                table.ajax.reload();

                swal("Data Telah Dihapus", {
                    icon: "success",
                });
            } else {
                swal("Penghapusan Batal");
            }
        });
}



init()