var table;

function init() {
    showForm(false);
    get_data();

    $("#formTambah").on("submit", function (e) {
        saveOrEdit(e);
    });
}

// Clean Form
function cleanString() {
    $("#id_fas").val("");
    $("#nama_fasilitas").val("");
    $("#ket").val("");
    $("#id_kamar").val("");
}

// Untuk menampilkan Form
function showForm(flag) {
    cleanString();
    if (flag) {
        $("#daftarKamar").hide();
        $("#formTambah").show();
        $("#btnTambah").hide();
    } else {
        $("#daftarKamar").show();
        $("#formTambah").hide();
        $("#btnTambah").show();
    }
}

// Untuk membatalkan showForm
function closeForm() {
    cleanString();
    showForm(false);
}

// Untuk menampilkan seluruh dataTable
function get_data(){
    table = $('#tbl_list').dataTable({
        "aProcessing" : true,
        "aServerSide" : true,
        "ajax" : {
            url : '../../controller/faskamar.php?action=get_data',
            type : "POST",
            dataType: "json",
            error: function (e){
                console.log(e.responseText);
            }
        },
        responsive: true
    }).DataTable();
}

function saveOrEdit(e) {
    e.preventDefault();
    var formData = new FormData($("#formTambah")[0]);

    if ($('#nama_fasilitas').val() == '' || $('#ket').val() == '' || $('#id_kamar').val() == ''){
        swal("Perhatian", "Silahkan Isi Data Terlebih Dahulu", "warning");
    } else {
        $.ajax({
            url: "../../Controller/faskamar.php?action=saveOrEdit",
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,

            success: function (data) {
                swal("Selamat", "Data Berhasil Disimpan", "success");
                showForm(false);
                table.ajax.reload();
            }
        });
    }
    cleanString();
}

function show(id_fas){
    $.post("../../Controller/faskamar.php?action=show", {id_fas: id_fas}, function (data){
        data = JSON.parse(data);
        showForm(true);

        $("#id_fas").val(data.id_fas);
        $("#nama_fasilitas").val(data.nama_fasilitas);
        $("#ket").val(data.ket);
        $("#id_kamar").val(data.id_kamar);

    })
}

function delete_data(id_fas){
    swal({
        title: "Konfirmasi Penghapusan Data",
        text: "Data Akan Dihapus Permanen!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
            if (willDelete) {
                $.post("../../Controller/faskamar.php?action=delete_data", {id_fas : id_fas},
                function(data) { })

                table.ajax.reload();

                swal("Data Telah Dihapus", {
                    icon: "success",
                });
            } else {
                swal("Penghapusan Batal");
            }
        });
}



init()