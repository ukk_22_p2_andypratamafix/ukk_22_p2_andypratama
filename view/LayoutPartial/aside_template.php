<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="../datapesan/datapesan.php">
            <i class="fa fa-bed"></i> <span>Data Pemesanan</span>
          </a>
        </li>
      </ul>
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="../Umum/fasumum.php">
            <i class="fa fa-book"></i> <span>Fasilitas Umum</span>
          </a>
        </li>
      </ul>
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="../fasilitaskamar/fasilitaskamar.php">
            <i class="fa fa-bed"></i> <span>tipe Kamar</span>
          </a>
        </li>
      </ul>
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="../faskamar/faskamar.php">
            <i class="fa fa-bed"></i> <span>Fasilitas Kamar</span>
          </a>
        </li>
      </ul>
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="../../logOut.php">
            <i class="fa fa-windows"></i> <span>Logout</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
</aside>