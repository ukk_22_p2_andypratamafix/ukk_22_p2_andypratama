<?php
  require_once("../../Config/connection.php");
  
  
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Blank Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php
    require_once("../lay2/head_template.php");
  ?>
</head>
  <body class="hold-transition skin-blue sidebar-mini">
   <!-- Head in Here -->
    
    <!-- ================================================== -->

    <!-- ============================================== -->
    <!-- Header In Here -->
    <?php
        require_once("../lay2/nav_template.php");
    ?>
    <!-- =============================================== -->
    <!-- Side In Here -->
    <?php
        require_once("../lay2/aside_template.php");
    ?>
    <!-- =============================================== -->
<!-- <div class="wrapper"> -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
    <!-- <div class="box">
        <div class="box-header">
              <h3 class="box-title">Data Kamar</h3>
              <button type="button" id="btnTambah" onclick="showForm(true)" class="btn btn-success pull-right">
              <i class="fa fa-plus"></i>Tambah
              </button>
        </div> -->
        <!-- /.box-header -->
            <div class="box-body" id="daftarKamar">
              <table id="tbl_list" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nama Kamar</th>                  
                  <th>Jumlah</th>
                  
                </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
        <!-- Form Tambah -->
         <!-- /.box-body -->
        <!-- Form Tambah -->
        <!-- <form id="formTambah" method="POST">
          <input type="hidden" name="id_kamar" id="id_kamar">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">TambahKamar</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
              <!-- <div class="box-body">
                
                <!-- Tipe Kamar -->
                <!-- <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Tipe Kamar</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="tipe_kamar" name="tipe_kamar" placeholder="">
                  </div>
                </div>
                <br>
                <p> -->
                <!-- Jumlah Kamar -->
                <!-- <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Jumlah Kamar</label>

                  <div class="col-sm-10">
                    <input type="number" class="form-control" id="jml" name="jml" placeholder="Jumlah">
                  </div>
                </div>
              </div> -->
              <!-- /.box-body -->
              <!-- <div class="box-footer">
                <button type="button" class="btn btn-danger" onclick="closeForm()">Batal</button>
                <button type="submit" class="btn btn-info" id="btnSimpan">Simpan</button>
              </div> -->
              <!-- /.box-footer -->
          <!-- </div>
        </form> -->
        <!-- End Form Tambah -->
    </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- </div> -->
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../../Public/1/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../Public/1/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../Public/1/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../Public/1/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../Public/1/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../Public/1/js/demo.js"></script>

<!-- DataTable -->
<script src="../../Public/1/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../Public/1/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Custom JS -->
<script type="text/javascript" src="fasres.js"></script>
<!-- SweetAlert -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</body>
</html>
