<?php
    require_once("../models/pemesanan.php");
    
    $pemesanan = new pemesanan();

    // Cek jika ada id_kamar,tipe_kamar,jml pada req
    // Jika ada jalankan metode cleanString
    // Jika tidak ada maka kosongkan
    $id_pemesanan = isset($_POST["id_pemesanan"]) ? cleanString($_POST["id_pemesanan"]): "";
    $email = isset($_POST["email"]) ? cleanString($_POST["email"]): "";
    $no_hp = isset($_POST["no_hp"]) ? cleanString($_POST["no_hp"]): "";
    $nm_tamu = isset($_POST["nm_tamu"]) ? cleanString($_POST["nm_tamu"]): "";
    $id_kamar = isset($_POST["id_kamar"]) ? cleanString($_POST["id_kamar"]): "";
    $cek_in = isset($_POST["cek_in"]) ? cleanString($_POST["cek_in"]): "";
    $cek_out = isset($_POST["cek_out"]) ? cleanString($_POST["cek_out"]): "";
    $jml = isset($_POST["jml"]) ? cleanString($_POST["jml"]): "";





    // Struktur Kendali CRUD
    switch ($_GET["action"]){
        case 'saveOrEdit' :
            if(empty($id_kamar)){
                // Jika id_kamar tidak ada pada req, jalankan method insert
                $response = $pemesanan->insert($email, $no_hp,$nm_tamu,$nm_tamu,$id_kamar,$cek_in,$cek_out,$jml);
            } else {
                // Jika id_kamar ada, jalankan method edit
                $response = $pemesanan->update($id_pemesanan, $email, $no_hp,$nm_tamu,$id_kamar,$cek_in,$cek_out,$jml);
            }
        break;

        case 'get_data' :
            $response = $pemesanan->get_data();

            $data = Array();

            while($row = $response->fetch_object()){
                $data[] = array(
                    "0"=>$row->nama_pemesan,
                    "1"=>$row->email,
                    "2"=>$row->no_hp,
                    "3"=>$row->nm_tamu,
                    "4"=>$row->cek_in,
                    "5"=>$row->cek_out,
                    "6"=>$row->jml,


                );
            }

            $result = array(
                "sEcho"=>1,
                "iTotalRecords"=>count($data),
                "aaData"=>$data
            );

            echo json_encode($result);
        break;
        case 'show' :
            $response = $pemesanan->show($id_pemesanan);
            echo json_encode($response);
        break;
        case 'delete_data' :
            $response = $pemesanan->delete_data($id_pemesanan);
        break;
    }