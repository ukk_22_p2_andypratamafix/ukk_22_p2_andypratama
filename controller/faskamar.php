<?php
    require_once("../models/faskamar.php");

    $faskamar = new faskamar();

    // Cek jika ada id_kamar,tipe_kamar,jml pada req
    // Jika ada jalankan metode cleanString
    // Jika tidak ada maka kosongkan
    $id_fas = isset($_POST["id_fas"]) ? cleanString($_POST["id_fas"]): "";
    $nama_fasilitas = isset($_POST["nama_fasilitas"]) ? cleanString($_POST["nama_fasilitas"]): "";
    $ket = isset($_POST["ket"]) ? cleanString($_POST["ket"]): "";
    $id_kamar = isset($_POST["id_kamar"]) ? cleanString($_POST["id_kamar"]): "";
    


    // Struktur Kendali CRUD
    switch ($_GET["action"]){
        case 'saveOrEdit' :
            if(empty($id_fas)){
                // Jika id_$id_fasilitas tidak ada pada req, jalankan method insert
                $response = $faskamar->insert($nama_fasilitas, $ket,$id_kamar);
            } else {
                // Jika id_$id_fasilitas ada, jalankan method edit
                $response = $faskamar->update($id_fas, $nama_fasilitas, $ket,$id_kamar);
            }
        break;

        case 'get_data' :
            $response = $faskamar->get_data();

            $data = Array();

            while($row = $response->fetch_object()){
                $data[] = array(
                    "0"=>$row->nama_fasilitas,
                    "1"=>$row->ket,
                    "2"=>$row->tipe_kamar,
                    "3"=>'<button class="btn btn-info btn-sm" onclick="show('.$row->id_fas.')" title="Edit Data"><i class="fa fa-pencil"></i></button>
                    <button class="btn btn-danger btn-sm" onclick="delete_data('.$row->id_fas.')"title="Delete Data"><i class="fa fa-trash"></i></button>'
                    
                );
            }

            $result = array(
                "sEcho"=>1,
                "iTotalRecords"=>count($data),
                "aaData"=>$data
            );

            echo json_encode($result);
        break;

        case 'show' :
            $response = $faskamar->show($id_fas);
            echo json_encode($response);
        break;
        case 'delete_data' :
            $response = $faskamar->delete_data($id_fas);
        break;
    }