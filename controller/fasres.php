<?php
    require_once("../models/fasres.php");

    $fasres = new fasres();

    // Cek jika ada id_kamar,tipe_kamar,jml pada req
    // Jika ada jalankan metode cleanString
    // Jika tidak ada maka kosongkan
    $id_kamar = isset($_POST["id_kamar"]) ? cleanString($_POST["id_kamar"]): "";
    $tipe_kamar = isset($_POST["tipe_kamar"]) ? cleanString($_POST["tipe_kamar"]): "";
    $fasilitas_kamar = isset($_POST["fasilitas_kamar"]) ? cleanString($_POST["fasilitas_kamar"]): "";

    // Struktur Kendali CRUD
    switch ($_GET["action"]){
        case 'get_data' :
            $response = $fasres->get_data();

            $data = Array();

            while($row = $response->fetch_object()){
                $data[] = array(
                    "0"=>$row->tipe_kamar,
                    "1"=>$row->jml,
                    
                );
            }

            $result = array(
                "sEcho"=>1,
                "iTotalRecords"=>count($data),
                "aaData"=>$data
            );

            echo json_encode($result);
        break;
    }