<?php
    require_once("../models/fashotel.php");

    $fashotel = new fashotel();

    // Cek jika ada id_kamar,tipe_kamar,jml pada req
    // Jika ada jalankan metode cleanString
    // Jika tidak ada maka kosongkan
    $id_fasilitas = isset($_POST["id_fasilitas"]) ? cleanString($_POST["id_fasilitas"]): "";
    $nama_fasilitas = isset($_POST["nama_fasilitas"]) ? cleanString($_POST["nama_fasilitas"]): "";
    $ket = isset($_POST["ket"]) ? cleanString($_POST["ket"]): "";

    // Struktur Kendali CRUD
    switch ($_GET["action"]){
        case 'get_data' :
            $response = $fashotel->get_data();

            $data = Array();

            while($row = $response->fetch_object()){
                $data[] = array(
                    "0"=>$row->nama_fasilitas,
                    "1"=>$row->ket,
                    
                );
            }

            $result = array(
                "sEcho"=>1,
                "iTotalRecords"=>count($data),
                "aaData"=>$data
            );

            echo json_encode($result);
        break;
    }