<?php require_once 'models/Kamar.php';

$kamar = new Kamar();
$result = $kamar->get_data();

  if(isset($_POST["submit"]) and $_POST["submit"]=="reservasi"){
      require_once("indexdata.php");
      $pemesanan = new Pemesanan();
      
      $nama_pemesan = $_POST['nama_pemesan'];
      $email = $_POST['email'];
      $no_hp = $_POST['no_hp'];
      $nm_tamu = $_POST['nm_tamu'];
      $id_kamar = $_POST['id_kamar'];
      $cek_in = $_POST['cek_in'];
      $cek_out = $_POST['cek_out'];
      $jml = $_POST['jml'];


      if(empty($nama_pemesan) AND empty($id_kamar)){


          $alert = "<script>alert('Silahkan Lengkapi Data');</script>";
          echo $alert;
      }else {
          $result = $pemesanan->insert($nama_pemesan,$email,$no_hp,$nm_tamu,$id_kamar,$cek_in,$cek_out,$jml);
          if(isset($result)){
              header("Location:".BASE_URL."bukti_pembayaran.php");
              exit();
          }
      }
  }
?>
<?php 
  require_once 'view/LayoutPartial/head.php';
  ?>

<body>
<?php 
  require_once 'view/LayoutPartial/nav.php';
  ?>
<!-- end:fh5co-header -->
<aside id="fh5co-hero" class="js-fullheight">
  <div class="flexslider js-fullheight">
    <ul class="slides">
       <li style="background-image: url(public/images/hotel1.jpg);">
         <div class="overlay-gradient"></div>
         <div class="container">
           <div class="col-md-12 col-md-offset-0 text-center slider-text">
             <div class="slider-text-inner js-fullheight">
               <div class="desc">
                 <p><span>Bora Hotel</span></p>
                 <h2>Reserve Room for Family Vacation</h2>
               </div>
             </div>
           </div>
         </div>
       </li>
       <li style="background-image: url(public/images/bg.jpg);">
         <div class="overlay-gradient"></div>
         <div class="container">
           <div class="col-md-12 col-md-offset-0 text-center slider-text">
             <div class="slider-text-inner js-fullheight">
               <div class="desc">
                 <p><span>Deluxe Hotel</span></p>
                 <h2>Make Your Vacation Comfortable</h2>
                 
               </div>
             </div>
           </div>
         </div>
       </li>
       <li style="background-image: url(public/images/hotel2.jpg);">
         <div class="overlay-gradient"></div>
         <div class="container">
           <div class="col-md-12 col-md-offset-0 text-center slider-text">
             <div class="slider-text-inner js-fullheight">
               <div class="desc">
                 <p><span>Luxe Hotel</span></p>
                 <h2>A Best Place To Enjoy Your Life</h2>
                 <p>
                   <a href="#form" class="btn btn-primary btn-lg">Pesan Sekarang</a>
                 </p>
               </div>
             </div>
           </div>
         </div>
       </li>
       
      </ul>
    </div>
</aside>
<div class="wrap">
  <div class="container">
    <div class="row">
      <div id="availability">
      </div>
    </div>
  </div>
</div> 
<div class="container">
  <div class="box-body">
    
  </div>
</div>
  
<div class="container">
<div class="row">
  <div class="col-md-12">
<br>
<br>
<br>
  <div class="box-header with-border center">
  <h3 class="box-title">Form Pemesanan</h3>
  </div>
  <form method="POST" class="form-horizontal" id="form">
  <div class="box-body">
              <!-- Nama Pemesan -->
              <div class="form-group">
                <label for="nama_pemesanan" class="col-sm-2 control-label">Nama Pemesan</label>
                <div class="col-sm-5">
                  <input type="text" class="form-control" name="nama_pemesan"  placeholder="Nama Pemesan">
                </div>
              </div>
              <!-- Email -->
              <div class="form-group">
                <label for="email" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-5">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
                </div>
              </div>
              <!-- No HP -->
              <div class="form-group">
                <label for="no_hp" class="col-sm-2 control-label">No Handphone</label>
                <div class="col-sm-5">
                  <input type="text" class="form-control" id="no_hp" name="no_hp" placeholder="No Handphone" required>
                </div>
              </div>
              <!-- Nama Tamu -->
              <div class="form-group">
                <label for="nama_tamu" class="col-sm-2 control-label">Nama Tamu</label>
                <div class="col-sm-5">
                  <input type="text" class="form-control" name="nm_tamu" id="nm_tamu" placeholder="Nama Tamu">
                </div>
              </div>
              <!-- Tipe Kamar -->
              <div class="form-group">
                <label class="col-sm-2 control-label">Tipe Kamar</label>
                <div class="col-sm-5">
                <select name="id_kamar" class="form-control">
                    <?php
                      while($row = $result->fetch_object()){
                    ?>
                    <option value="<?php echo $row->id_kamar ?>"><?php echo $row->tipe_kamar ?></option>
                    <?php
                      }
                    ?>
                </select>
                </div>
              </div>
              <!-- Check IN dan Check Out -->
              <div class="form-group">
                <label for="CheckIn" class="col-sm-2 control-label">CheckIn</label>
                <div class="col-sm-5">
                  <input type="date" class="form-control" name="cek_in" id="cek_in" required>
                  <i class="fa fa-calender"></i>
                </div>
              </div>
              <div class="form-group">
                <label for="CheckIn" class="col-sm-2 control-label">Check Out</label>
                <div class="col-sm-5">
                  <input type="date" class="form-control" name="cek_out" id="cek_out">
                  <i class="fa fa-calender"></i>
                </div>
                <div>
              </div>
              <!-- Jumlah Kamar -->
              <div class="form-group">
                <label for="jml_kamar" class="col-sm-2 control-label">Jumlah Kamar</label>
                <div class="col-sm-2">
                  <input type="number" class="form-control" name="jml" placeholder="Jumlah Kamar">
                </div>
              </div>
                      
              <!-- /.box-body -->
              <div class="box-footer">
                <input type="hidden" name="submit" value="reservasi">
                <button type="submit" class="btn btn-info pull-right">Pesan Sekarang</button>
              </div>
              <!-- /.box-footer -->
          </div>
      </form>
  </div>
</div>
</div>

</div>
<!-- END fh5co-page -->

</div>
<!-- END fh5co-wrapper -->

<!-- Javascripts -->
<script src="public/js/jquery-2.1.4.min.js"></script>
<!-- Dropdown Menu -->
<script src="public/js/hoverIntent.js"></script>
<script src="public/js/superfish.js"></script>
<!-- Bootstrap -->
<script src="public/js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="public/js/jquery.waypoints.min.js"></script>
<!-- Counters -->
<script src="public/js/jquery.countTo.js"></script>
<!-- Stellar Parallax -->
<script src="public/js/jquery.stellar.min.js"></script>
<!-- Owl Slider -->
<!-- // <script src="public/js/owl.carousel.min.js"></script> -->
<!-- Date Picker -->
<script src="public/js/bootstrap-datepicker.min.js"></script>
<!-- CS Select -->
<script src="public/js/classie.js"></script>
<script src="public/js/selectFx.js"></script>
<!-- Flexslider -->
<script src="public/js/jquery.flexslider-min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script src="public/js/custom.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</body>
</html>
