<?php
    require_once 'models/Pemesanan.php';
    $pemesanan = new Pemesanan();
    $result = $pemesanan->get_data();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bukti Reservasi</title>
</head>
<body>
    <h1>Bukti Reservasi</h1>
    <?php
        $data = mysqli_fetch_row($result);
    ?>
    <p>Nama Pemesan : <?php echo $data['1']?></p>
    <p>Email: <?php echo $data['2']?></p>
    <p>No. HP : <?php echo $data['3']?></p>
    <p>Nama Tamu : <?php echo $data['4']?></p>

    <table border="1" style="width: 100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Tipe Kamar</th>
                <th>Check IN</th>
                <th>Check OUT</th>
                <th>Jumlah</th>
            </tr>
        </thead>
        <tbody>
        <?php
            $no =1;
            while($row = mysqli_fetch_array($result)){
        ?>
         <tr>
                <td><?php echo $no++ ?></td>
                <td><?php echo $row['tipe_kamar']?></td>
                <td><?php echo $row['cek_in']?></td>
                <td><?php echo $row['cek_out'] ?></td>
                <td><?php echo $row['jml'] ?></td>
        </tr>
        <?php
            }
        ?> 
           
        </tbody>
    </table>
    
</body>
</html>