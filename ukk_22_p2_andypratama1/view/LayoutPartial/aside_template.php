<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="../admin/tipekamar.php">
            <i class="fa fa-bed"></i> <span>Data Reservasi</span>
          </a>
        </li>
      </ul>
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="../admin/fasilitaskamar/fasilitaskamar.php">
            <i class="fa fa-bed"></i> <span>Fasilitas Kamar</span>
          </a>
        </li>
      </ul>
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="../datapesan/datapesan.php">
            <i class="fa fa-bed"></i> <span>Data Pesan</span>
          </a>
        </li>
      </ul>
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="../../logOut.php">
            <i class="fa fa-windows"></i> <span>Logout</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
</aside>